# GitLabDemoUpstream

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://tkf.gitlab.io/GitLabDemoUpstream.jl/dev)
[![Build Status](https://github.com/tkf/GitLabDemoUpstream.jl/badges/master/pipeline.svg)](https://github.com/tkf/GitLabDemoUpstream.jl/pipelines)
[![Coverage](https://github.com/tkf/GitLabDemoUpstream.jl/badges/master/coverage.svg)](https://github.com/tkf/GitLabDemoUpstream.jl/commits/master)
