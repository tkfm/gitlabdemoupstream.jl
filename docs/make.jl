using GitLabDemoUpstream
using Documenter

makedocs(;
    modules=[GitLabDemoUpstream],
    authors="Takafumi Arakaki <aka.tkf@gmail.com> and contributors",
    repo="https://github.com/tkf/GitLabDemoUpstream.jl/blob/{commit}{path}#L{line}",
    sitename="GitLabDemoUpstream.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://tkf.gitlab.io/GitLabDemoUpstream.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
